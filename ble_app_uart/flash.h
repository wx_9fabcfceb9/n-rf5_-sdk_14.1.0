#ifndef _FLASH_H_
#define _FLASH_H_

#include "stdint.h"
#pragma pack(4)
typedef struct{

	uint8_t rs485_addr;
	uint8_t rs485_addr1;
	uint8_t rs485_addr2;
	uint8_t rs485_addr3;
	uint16_t CRC1;
  uint16_t CRC;
}SaveValueStruct;
#pragma pack()

extern SaveValueStruct SaveValue[2];
void LoadCfg(void);
void SaveCfg(void);

#endif
