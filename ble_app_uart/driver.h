#ifndef _GIDRIVER_H_
#define _GIDRIVER_H_
#include <stdint.h>


#define PIN_COUNT 28


#define BIT(x)  (1<<x)
extern uint8_t ctrsig_value[8];

void PIN_SET(uint32_t pin,uint8_t hight);
void ctr_out(uint8_t port,uint8_t hight);
void ctr_flush(void);
void ctr_init(void);
void sig_init(void);
void exit_count_init(void);
#endif
