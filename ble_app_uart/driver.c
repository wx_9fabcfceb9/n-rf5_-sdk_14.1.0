#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
#include "nrf_log.h"
#include "driver.h"

const uint8_t ctr_port_code[]={24,23,22,21,17,18,19,20};
const uint8_t sig_port_code[]={16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
uint8_t ctrsig_value[8];

void PIN_SET(uint32_t pin,uint8_t hight)
{
	if(hight)
	{
		NRF_GPIO->DIRSET=(1<<pin);
		NRF_GPIO->OUTSET=(1<<pin);
	}else
	{
		NRF_GPIO->DIRSET=(1<<pin);
		NRF_GPIO->OUTCLR=(1<<pin);
	}
}
void ctr_init()
{
		for(int i=0;i<sizeof(ctrsig_value);i++)
	{
		ctrsig_value[i]&=~BIT(0);
	}
	ctr_flush();
}
void sig_get()
{
	uint8_t temp;
	for(int i=0;i<sizeof(ctrsig_value);i++)
	{
		temp=nrf_gpio_pin_read(sig_port_code[i*2]);
		if(temp)ctrsig_value[i]|=BIT(1);else ctrsig_value[i]&=~BIT(1);
		temp=nrf_gpio_pin_read(sig_port_code[i*2+1]);
		if(temp)ctrsig_value[i]|=BIT(2);else ctrsig_value[i]&=~BIT(2);
	}
	//NRF_LOG_HEXDUMP_INFO(ctrsig_value,sizeof(ctrsig_value));
}
void sig_init()
{
		for(int i=0;i<sizeof(ctrsig_value)*2;i++)
	{
		nrf_gpio_pin_dir_set(sig_port_code[i],NRF_GPIO_PIN_DIR_INPUT);
	}
	sig_get();
	
}
void ctr_out(uint8_t port,uint8_t hight)
{
	PIN_SET(ctr_port_code[port],hight);
}
void ctr_flush()
{
	for(int i=0;i<sizeof(ctrsig_value);i++)
	{
		ctr_out(i,ctrsig_value[i]&BIT(0));
	}
}

void exit_count_init()
{
 nrf_gpio_cfg_input(PIN_COUNT, NRF_GPIO_PIN_PULLDOWN);
	NVIC_DisableIRQ(GPIOTE_IRQn);
	NVIC_ClearPendingIRQ(GPIOTE_IRQn);
	NVIC_SetPriority(GPIOTE_IRQn, 3); //optional: set priority of interrupt
	NVIC_EnableIRQ(GPIOTE_IRQn);
	NRF_GPIOTE->CONFIG[0] =  (GPIOTE_CONFIG_POLARITY_LoToHi << GPIOTE_CONFIG_POLARITY_Pos)
										 | (PIN_COUNT << GPIOTE_CONFIG_PSEL_Pos)   //  ??? ??
										 | (GPIOTE_CONFIG_MODE_Event << GPIOTE_CONFIG_MODE_Pos);
	NRF_GPIOTE->INTENSET  = GPIOTE_INTENSET_IN0_Set << GPIOTE_INTENSET_IN0_Pos;
	NRF_GPIO->PIN_CNF[PIN_COUNT] = (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);

}
uint32_t count_init=0;
#include "nrf_log_ctrl.h"
void GPIOTE_IRQHandler(void)
{

    if ((NRF_GPIOTE->EVENTS_IN[0] == 1) && 
        (NRF_GPIOTE->INTENSET & GPIOTE_INTENSET_IN0_Msk))
    {
        NRF_GPIOTE->EVENTS_IN[0] = 0; //�ж��¼�����.
			//NRF_LOG_INFO("GPIOTE_IRQHandler!=%d",nrf_gpio_pin_read(PIN_COUNT));
			UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());
			count_init++;
    }
 
}

