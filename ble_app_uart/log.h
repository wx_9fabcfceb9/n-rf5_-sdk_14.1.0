#ifndef _LOG_H_
#define _LOG_H_


#include "nrf_log_ctrl.h"

#define NRF_LOG(...)  {NRF_LOG_INTERNAL_INFO( __VA_ARGS__);UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());}
#define NRF_LOG_HEX(...) {NRF_LOG_INTERNAL_HEXDUMP_INFO( __VA_ARGS__);UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());}
#endif
