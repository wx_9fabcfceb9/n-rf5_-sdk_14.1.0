#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "driver.h"
#include "ads1115.h"
#include "log.h"
#include "flash.h"

#define MIBUD_HEAD 0xfb
#define MIBUD_END 0xfc
#define MIBUD_ECH 0xfd
#define BROADCAST_ADDR 255
uint8_t rx_buffer[256],rx_index=0;
uint8_t cmd_buffer[256],cmd_index=0;
extern uint32_t count_init;


void uart_write(uint8_t *buf,uint8_t i)
{
	uint8_t temp;
	app_uart_put(MIBUD_HEAD);
	while(i--)
	{
		temp=*buf++;
		switch(temp)
		{
			case MIBUD_HEAD:app_uart_put(MIBUD_ECH);app_uart_put(0x0b);break;
			case MIBUD_END:app_uart_put(MIBUD_ECH);app_uart_put(0x0c);break;
			case MIBUD_ECH:app_uart_put(MIBUD_ECH);app_uart_put(0x0d);break;
			default:app_uart_put(temp);break;
		}
		
	}
	app_uart_put(MIBUD_END);
}
void uart_exe(uint8_t *buf,uint8_t l)
{
	uint8_t tx_buffer[256],i=0;
	if(buf[0]==SaveValue[0].rs485_addr&&SaveValue[0].rs485_addr!=BROADCAST_ADDR)
	{
		tx_buffer[i++]=buf[0];
		tx_buffer[i++]=buf[1];
		switch(buf[1])
		{
			case 0x01:sig_get();for(int z=0;z<sizeof(ctrsig_value);z++)tx_buffer[i++]=ctrsig_value[z];uart_write(tx_buffer,i);break;//获取继电器和接触器信号
			case 0x02:for(int z=0;z<sizeof(ctrsig_value);z++)if(buf[z+2])ctrsig_value[z]|=BIT(0);else ctrsig_value[z]&=~BIT(0);ctr_flush(); uart_write(tx_buffer,i);break;//设置继电器信号
			case 0x03:i+=ads115_read(0,&tx_buffer[i]);i+=ads115_read(1,&tx_buffer[i]);uart_write(tx_buffer,i);break;//设置继电器信号
			case 0x04:tx_buffer[i++]=(count_init>>24)&0xff;tx_buffer[i++]=(count_init>>16)&0xff;tx_buffer[i++]=(count_init>>8)&0xff;tx_buffer[i++]=(count_init)&0xff;uart_write(tx_buffer,i);break;
			default:uart_write(buf,l);break;
		}
	}else if(buf[0]==BROADCAST_ADDR)
	{
		tx_buffer[i++]=buf[0];
		tx_buffer[i++]=buf[1];
		switch(buf[1])
		{
			case 0x01:tx_buffer[i++]=SaveValue[0].rs485_addr;tx_buffer[i++]=SaveValue[0].rs485_addr=buf[2];uart_write(tx_buffer,i);SaveCfg();break;
			case 0x02:tx_buffer[i++]=SaveValue[0].rs485_addr;uart_write(tx_buffer,i);break;
			default:uart_write(tx_buffer,i);break;
		}
	}
	NRF_LOG_HEXDUMP_INFO(buf,l);
//	uart_write(buf,l);
//	if(cmd_index==0)
//	{
//		for(int i=0;i<l;i++)cmd_buffer[i]=buf[i];
//		cmd_index=l;
//	}
}
void uart_event(uint8_t data)
{
	static uint8_t task_flag=0;
	if(rx_index<sizeof(rx_buffer))
	{
		if(data==MIBUD_HEAD){rx_index=0;return;}
		if(task_flag==MIBUD_ECH)
		{
			switch(data)
			{
				case 0x0b:rx_buffer[rx_index++]=MIBUD_HEAD;return;
				case 0x0c:rx_buffer[rx_index++]=MIBUD_END;return;
				case 0x0d:rx_buffer[rx_index++]=MIBUD_ECH;return;
			}
			task_flag=0;
	  }
		if(data==MIBUD_END)
		{
			uart_exe(rx_buffer,rx_index);
			rx_index=0;
			return;
		}
		if(data==MIBUD_ECH)
		{
			task_flag=MIBUD_ECH;
		}
		else rx_buffer[rx_index++]=data;
		
	}
}
void mibus_task()
{
	if(cmd_index>0)
	{
		//uart_write(cmd_buffer,cmd_index);
		//cmd_index=0;
	}
}

