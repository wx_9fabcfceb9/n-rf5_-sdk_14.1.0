#ifndef _GEEKINSECT_V2_H
#define _GEEKINSECT_V2_H


#define LED_EYE1 06
#define LED_EYE2 13
#define BEEP     00
#define ADC_RANGE1 03
#define ADC_RANGE2 30
#define ADC_LINE1 02
#define ADC_LINE2 29
#define ADC_LINE3 04
#define ADC_BAT 03
#define MOTA_1A  25
#define MOTA_1B  26
#define MOTA_2A  27
#define MOTA_2B  28
#define WS_PIN1  31
#define WS_PIN2  17
#define WS_PIN3  12
#define IR_PIN   01
#define POW_EN   14
#define POW_KEY  15
#define USER_KEY  19
#define BAT_STDBY 16
#define BAT_CHRG  18

#endif
