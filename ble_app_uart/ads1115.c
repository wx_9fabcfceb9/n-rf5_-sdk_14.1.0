


/* Copyright (c) 2009 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf.h"
#include "bsp.h"
#include "nrf_drv_twi.h"
#include "ads1115.h"
#include "nrf_log.h"
#include "log.h"
#include <math.h>

#define ads1115_SCL_PIN             30    // SCL signal pin
#define ads1115_SDA_PIN             31    // SDA signal pin	

#define TWI_INSTANCE_ID  0

static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);


static uint8_t       m_device_address=0x49;          // !< Device address in bits [7:1]


void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_config = {
       .scl                = ads1115_SCL_PIN,
       .sda                = ads1115_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_MID,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}

bool ads1115_init_(uint8_t device_address)
{   
    bool transfer_succeeded = true;

		return transfer_succeeded;
}



bool ads1115_register_write(uint8_t register_address, uint8_t value)
{
	  uint32_t err_code;
	  
	  uint8_t w2_data[2];

    w2_data[0] = register_address;
    w2_data[1] = value;
	  err_code = nrf_drv_twi_tx(&m_twi, m_device_address, w2_data, 2,false);
	  APP_ERROR_CHECK(err_code);

	  return true;
}

bool ads1115_register_read(uint8_t register_address, uint8_t * destination, uint8_t number_of_bytes)
{

	  uint32_t err_code;
	
    err_code = nrf_drv_twi_tx(&m_twi, m_device_address, &register_address, 1,false);
	  APP_ERROR_CHECK(err_code);
	  err_code = nrf_drv_twi_rx(&m_twi, m_device_address, destination, number_of_bytes);
	  APP_ERROR_CHECK(err_code);
	
    return true;
}

/**********************************************************************************************
 * 描  述 : 写入多个字节
 * 入  参 : 无
 * 返回值 : 无
 ***********************************************************************************************/
bool ads1115_register_write_len(uint8_t register_address,uint8_t *buf, uint8_t len)
{
    uint32_t err_code;
	  uint8_t w2_data[50],i;
    w2_data[0] = register_address;
	  for(i=0;i<len;i++)w2_data[i+1] = *(buf+i);
	  err_code = nrf_drv_twi_tx(&m_twi, m_device_address, w2_data, len+1,false);
	  APP_ERROR_CHECK(err_code);
	  return err_code;
}
bool ads1115_register_read_len(uint8_t register_address,uint8_t * destination , uint8_t number_of_bytes)
{
	  uint32_t err_code;
	
	  err_code = nrf_drv_twi_tx(&m_twi, m_device_address, &register_address, 1,false);
	  APP_ERROR_CHECK(err_code);
	  err_code = nrf_drv_twi_rx(&m_twi, m_device_address, destination, number_of_bytes);
	  APP_ERROR_CHECK(err_code);
	
	  return err_code;
}



void ads1115_init()
{
	twi_init();
	NRF_LOG_INFO("ads1115_Init");
	uint8_t r_data[10];
	uint8_t w_data[10]={0x64,0x83};
	ads1115_register_write_len(0x01,w_data,2);
	nrf_delay_ms(100);
  ads1115_register_read_len(0x01,r_data,2);
	NRF_LOG_HEXDUMP_INFO(r_data,2);
	w_data[0]=0x00;
	w_data[1]=0x00;
	ads1115_register_write_len(0x02,w_data,2);
	w_data[2]=0xff;
	w_data[3]=0xff;
//	ads1115_register_write_len(0x03,w_data,2);
}
//000 : AINP = AIN0 and AINN = AIN1 (default)
//001 : AINP = AIN0 and AINN = AIN3
//010 : AINP = AIN1 and AINN = AIN3
//011 : AINP = AIN2 and AINN = AIN3
//100 : AINP = AIN0 and AINN = GND
//101 : AINP = AIN1 and AINN = GND
//110 : AINP = AIN2 and AINN = GND
//111 : AINP = AIN3 and AINN = GND
//000 : FSR = ±6.144 V
//001 : FSR = ±4.096 V
//010 : FSR = ±2.048 V (default)
//011 : FSR = ±1.024 V
//100 : FSR = ±0.512 V
//101 : FSR = ±0.256 V
//110 : FSR = ±0.256 V
//111 : FSR = ±0.256 V

uint8_t ads115_read(uint8_t ch,uint8_t *buf)
{
	uint8_t w_data[2]={0x54,0xF3},r_data[4]={0x00,0x00,0x00,0x00};
	switch(ch)
	{
		case 0:w_data[0]=0x44;break;
		case 1:w_data[0]=0x54;break;
	}
	ads1115_register_write_len(0x01,w_data,2);
	ads1115_register_read_len(0x01,r_data,2);
	nrf_delay_ms(10);
	ads1115_register_read_len(0x00,&r_data[2],2);
	NRF_LOG_HEX(r_data,4);
	buf[0]=r_data[2];
	buf[1]=r_data[3];
	return 2;
}

void ads1115_test()
{ 
	uint8_t r_data[10];
	ads115_read(0,r_data);
	nrf_delay_ms(200);
	ads115_read(1,r_data);
	nrf_delay_ms(200);
}