#include "nrf_nvmc.h"
#include <stdbool.h>
#include <stdint.h>
#include "crc16.h"
#include "nrf_fstorage.h"
#include "nrf_fstorage_nvmc.h"
#include "nrf_fstorage_sd.h"
#include "nrf_delay.h"
#include "nrf_soc.h"
#include "nrf_log.h"
#include "flash.h"
#include "log.h"
//?
#define PAGE_NUM1         100
#define PAGE_NUM2         101
#define PAGE_NUM3         102
//???
#define FLASH_START_ADDR1 (4096*PAGE_NUM1)
#define FLASH_START_ADDR2 (4096*PAGE_NUM2)
#define FLASH_START_ADDR3 (4096*PAGE_NUM2)

//#define FLASH_START_ADDR1 0x73000-4096-4096
//#define FLASH_START_ADDR2 0x73000-4096



SaveValueStruct SaveValue[2];

bool FLASH_BUSY=false;


void goBoot()
{
	sd_flash_page_erase(PAGE_NUM3);
	nrf_delay_ms(200);
}

bool LoadConf(SaveValueStruct *V)
{
	int i,l;
  uint8_t *D=(uint8_t *)V;
	uint16_t crc;
	__IO uint32_t addr=FLASH_START_ADDR1;
	__IO uint8_t *S=((uint8_t *)addr);
	l=sizeof(SaveValueStruct);
	for(i=0;i<l;i++)*D++=*S++;
	crc=crc16_compute((uint8_t const *)V,sizeof(SaveValueStruct)-2,0);
	if(crc==V->CRC)NRF_LOG("LOAD1=%x,CRC=%x=CRC=%x",addr,crc,V->CRC);
	if(crc!=V->CRC)NRF_LOG("LOAD1=%x,CRC=%x!=CRC=%x",addr,crc,V->CRC);
	if(crc!=V->CRC)
	{
		addr=FLASH_START_ADDR2;
		S=((uint8_t *)addr);
		D=(uint8_t *)V;
		for(i=0;i<l;i++)*D++=*S++;
		crc=crc16_compute((uint8_t const *)V,sizeof(SaveValueStruct)-2,NULL);
	  if(crc==V->CRC)NRF_LOG("LOAD2=%x,CRC=%x=CRC=%x",addr,crc,V->CRC);
	  if(crc!=V->CRC)NRF_LOG("LOAD2=%x,CRC=%x!=CRC=%x",addr,crc,V->CRC);
		if(crc!=V->CRC)
		{
			////do thing
			return false;
		}
	}
	return true;
}
bool WriteErase(uint32_t page)
{
	uint32_t ret;
R:
	ret=sd_flash_page_erase(page);
	if(ret==NRF_SUCCESS)return NRF_SUCCESS;
	else if(ret==NRF_ERROR_BUSY)
	{
		goto R;
	}
	nrf_delay_ms(2000);
	return false;
}
bool WriteSpase(uint32_t * p_dst, uint32_t * p_src, uint32_t size)
{
	uint32_t ret;
R:
	ret=sd_flash_write(((uint32_t *)p_dst),(uint32_t *)p_src,size);
	if(ret==NRF_SUCCESS)return NRF_SUCCESS;
	else if(ret==NRF_ERROR_BUSY)
	{
		goto R;
	}else
	{
		NRF_LOG("sd_flash_write=%d,%d",ret,p_src);
	}
	nrf_delay_ms(2000);
	return false;
}
void WriteConf(SaveValueStruct *V)
{
	
	uint32_t addr=FLASH_START_ADDR1;
	FLASH_BUSY=true;
    WriteErase(PAGE_NUM1);
	V->CRC=crc16_compute((uint8_t const *)V,sizeof(SaveValueStruct)-2,NULL);
	WriteSpase(((uint32_t *)addr),(uint32_t *)V,sizeof(SaveValueStruct));
	NRF_LOG("SAVE1=%x,CRC=%x",addr,V->CRC);
	nrf_delay_ms(200);
	WriteErase(PAGE_NUM2);
	addr=FLASH_START_ADDR2;
	WriteSpase(((uint32_t *)addr),(uint32_t *)V,sizeof(SaveValueStruct));
	NRF_LOG("SAVE2=%x,CRC=%x",addr,V->CRC);
	nrf_delay_ms(200);
	FLASH_BUSY=false;
}
void LoadCfg()
{
	LoadConf(&SaveValue[0]);
	NRF_LOG("SaveValue.rs485_addr:%d",SaveValue[0].rs485_addr);
}
void SaveCfg()
{
	WriteConf(&SaveValue[0]);
}
