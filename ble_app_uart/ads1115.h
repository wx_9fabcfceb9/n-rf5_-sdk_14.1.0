#ifndef _ADS1115_H_
#define _ADS1115_H_
#include "stdint.h"

void ads1115_init(void);
void ads1115_test(void);
bool ads1115_register_read(uint8_t register_address, uint8_t * destination, uint8_t number_of_bytes);
uint8_t ads115_read(uint8_t ch,uint8_t *buf);
#endif
